
interface abc
{
    fun show()
}

class xyz:abc
{
    override fun show() {
        println("abc")
    }
}

class www:abc
{
    override fun show() {
        println("www")
    }
}

fun main(args: Array<String>) {

    xyz().show()
    www().show()
}
